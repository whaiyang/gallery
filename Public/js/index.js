var frameWidth = 760;
var frames = 5;
var globalWait = 5000;
var offsetX = -3040;
var leftBtn, rightBtn, imgShowUl; 
var globalT;

var prevX, nextX, leftT, rightT;
var movePx = 20;
var li = 1;

function init(){
    leftBtn = document.getElementById("leftBtn");
	rightBtn = document.getElementById("rightBtn");	
	imgShowUl = document.getElementById("imgShowUl");
	globalT = setInterval("rightAnimate()",globalWait);
}

function leftMove(prevX){                  //左偏移movePx
  if( (offsetX - movePx) < prevX ){  
    clearInterval(leftT);
	offsetX = prevX ; 	
	li = 1;	
	globalT = setInterval("rightAnimate()", globalWait);  
  }	
  else{                                    
    offsetX -= movePx;
  }
  imgShowUl.style.left = offsetX + "px";		  
}

function rightMove(nextX){                 //右偏移movePX               
  if( (offsetX + movePx) > nextX ){
    clearInterval(rightT);
	offsetX = nextX ;
	li = 1;
	globalT = setInterval("rightAnimate()", globalWait);    
  }	
  else{
    offsetX += movePx;  
  }
  imgShowUl.style.left = offsetX + "px";
}

function leftAnimate(){                    //左推动至prevX
  if(li==1){
	clearInterval(globalT);
  	if(offsetX != -3040){
	  prevX = offsetX - frameWidth; 		  
	  leftT = setInterval("leftMove(prevX)", 20 );
	  li=0;	 
  	}
	else{
	  offsetX = 0;
	  imgShowUl.style.left = offsetX + "px";
	  globalT = setInterval("rightAnimate()", globalWait);  
	}
  }
}

function rightAnimate(){                   //右推动至nxetX 
  if(li==1){
	clearInterval(globalT);  
    if(offsetX != 0){
      nextX = offsetX + frameWidth;
	  rightT = setInterval("rightMove(nextX)", 20 ); 
	  li=0;
	}
	else{
	  offsetX = -3040;
	  imgShowUl.style.left = offsetX + "px";	
	  globalT = setInterval("rightAnimate()", globalWait);  
	}	  
  }
}
	
